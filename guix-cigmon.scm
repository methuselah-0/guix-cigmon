;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;; This file incorporates work covered by the Guix project's copyright
;; Pieces of code have been liberally copied, used or modified from
;; various Guix modules

(define-module (guix-cigmon)
  #:use-module (ice-9 match)
  #:use-module (guix build utils)  ;; substitute*
  #:use-module (guix packages)  ;; package-<x>
  #:use-module (gnu packages)   ;; fold-packages
  #:use-module (guix discovery) ;; fold-module-public-variables etc.
  #:use-module (guix diagnostics)
  #:use-module (guix git-download) ;; git-reference
  #:use-module (ice-9 regex)       ;; string-match etc
  #:use-module (ice-9 rdelim)      ;; read-line etc.  
  #:use-module (srfi srfi-1)
  #:use-module (config)
  #:use-module (config licenses)
  #:use-module (config api)
  #:use-module (config parser sexp)
  #:export ())

;;(use-modules (ice-9 match) (guix packages) (gnu packages) (guix discovery) (guix diagnostics) (guix git-download) (ice-9 regex) (ice-9 rdelim) (srfi srfi-1) (config) (config licenses) (config api) (config parser sexp))

(define config
  ;; Define our root configuration
  (configuration
   (name 'guix-package-mon)
   (version "0.0.1")
   (author "David Larsson")
   (license gpl3+)
   (copyright '(2020 2021))
   (keywords
    (list
     ;; Switch to force writing non-eager configuration files
     (switch
      (name 'write) (default #f) (test boolean?) (character #f)
      ;; Boolean Switch ---^-----------^          ^--- No single character
      (synopsis "Write configuration file in local dir."))
     (switch
      (name 'background) (default #f)
      ;; We declare this be passed as string and be turned into a number.
      (test boolean?) (example "#f")
      (synopsis "run in the background"))

     (switch
      (name 'git-commit-manifest) (default #f)(character #f)
      (test boolean?) (example "#f")
      (synopsis "apply 'git add <manifest>; git commit -m \"guix-cigmon\"' in the manifest directory"))

     (switch
      (name 'git-commit-packages) (default #f)(character #f)
      (test boolean?) (example "#f")
      (synopsis "apply 'git add <package-modules-dir>; git commit -m \"guix-cigmon\"' in the package-modules directory"))

     (switch
      (name 'git-push-manifest) (default #f)(character #f)
      (test boolean?) (example "#f")
      (synopsis "apply 'git push' in the manifest directory"))
     
     (switch
      (name 'git-push-packages) (default #f)(character #f)
      (test boolean?) (example "#f")
      (synopsis "apply 'git push' in the package-modules directory"))

     ;; A setting in the configuration file, if it exists.
     (setting
      (name 'interval) (default 60)
      (handler string->number)
      (test number?) (example "60")
      (synopsis "Time to wait in between looking for git updates"))

     (setting
      (name 'package-modules) (default "")(character #\d)
      (test string?) (example "/srv/git/my-guix-packages/python-extra-packages /srv/git/my-guix-packages/ruby-extra-packages")
      (synopsis "Package modules directory to be loaded that contains the packages you want monitored"))

     (setting
      (name 'manifest) (default "")
      (handler identity) (test string?)
      (description "A filepath to a manifest.scm file where you want (define cigmon-packages (list <cigmon-generated-pkg> ..)) to be added (and replaced on subsequent runs)")
      (synopsis "A filepath to a manifest.scm file"))
     
     (setting
      (name 'packages) (default "")
      (handler identity) (test string?)
      (synopsis "A string that is a whitespace separated list of package-name@package-branch")
      (description "A whitespace separated list of package-name@package-branch"))))

   (synopsis "Create a CI setup for guix packages with cuirass")
   (description "guix-cigmon monitors a set of packages at their git-location when given a dir-path to package modules and a set of <package-name>@<branch> as input. When it finds a new commit for a package name, a new package module named <my-package>-revisions.scm will be created (or appended to) next to the module it's originally defined in, which contains an inherited version of the original package. The new package gets defined as <my-package-1234567>. This generated package has the most recent commit of its <branch> and a correct hash. Thus, for each package revision update, if you've elected to invoke git commit and git push, guix-cigmon can trigger a rebuild of your list of packages if you also have a cuirass instance that listens to updates in the package modules repository.")

   (parser sexp-parser)
   ;; Specify where we want to install configuration files
   (directory (list
               ;; In the user's home directory, under .hello-world.  This one
               ;; is eager.
               (in-home ".guix-package-mon")
               ;; In the directory in which the command is invoked.  This one
               ;; is lazy.
               (in-cwd ".config/")))))

(define (package-url package) (git-reference-url (origin-uri (package-source package))))

(define (mktemp-dir) (let ((mydir (tmpnam))) (mkdir mydir) mydir))

;; From terminal, the procedure is like:
;; mkdir -d /tmp/test.XXXXX
;; cd /tmp/test.XXXXX
;; git init .
;; git add origin <package-url>
;; git fetch --depth=1 origin
;; (define latest-commit (cat .git/refs/remotes/origin/<branch>))
;; TODO: maybe take pkg and branch?
(define (package-latest-commits pkg)
  (let ((url (package-url pkg))
        (tmp-dir (mktemp-dir))
        (current-dir (getcwd)))
    (system* "git" "init" tmp-dir)
    (chdir tmp-dir)
    (system* "git" "remote" "add" "origin" url)
    (system* "git" "fetch" "--depth=1" "origin")
    (system (string-append "cat " tmp-dir "/.git/refs/remotes/origin/* > " tmp-dir "/all_commits.txt"))
    (define myport (open-input-file (string-append tmp-dir "/all_commits.txt")))
    (define lines (string-split (read-string myport) #\newline))
    (close-port myport)
    (system (string-append "rm -rf " tmp-dir))
    (chdir current-dir)
    lines))

(define (package-hash url commit)
  "Calculates the sha256 hash of the repo, and returns its base32 representation"
  (let ((tmp-dir (mktemp-dir))
	;; TODO: FIXME use proper tempfile instead of /tmp/package_hash
;;	(tmp-file ())
        (current-dir (getcwd)))
    ;; TODO: FIXME: recursive is hardcoded here, but this should be checked instead.
    (system* "git" "clone" "--recurse-submodules" url tmp-dir)
    (chdir tmp-dir)
    (system* "git" "checkout" commit)
    (system (string-append "guix hash -rx " tmp-dir " > " "/tmp/package_hash.txt"))
    (define myport (open-input-file (string-append "/tmp" "/package_hash.txt")))
    (define pkghash (read-line myport))    
    (close-port myport)
    (chdir current-dir)
    (system (string-append "rm -rf " tmp-dir))
    pkghash))

(define (package-latest-commit-for-branch pkg branch)
  (let ((url (package-url pkg))
        (tmp-dir (mktemp-dir))
        (current-dir (getcwd)))
    (system* "git" "init" tmp-dir)
    (chdir tmp-dir)
    (system* "git" "remote" "add" "origin" url)
    (system* "git" "fetch" "--depth=1" "origin")
    (system (string-append "cat " tmp-dir "/.git/refs/remotes/origin/" branch " > " tmp-dir "/all_commits.txt"))
    (define myport (open-input-file (string-append tmp-dir "/all_commits.txt")))
    (define line (car (string-split (read-string myport) #\newline)))
    (close-port myport)
    (system (string-append "rm -rf " tmp-dir))
    (chdir current-dir)
    line))

(define (mypkg? package mypkgname)
  (equal? (package-name package) mypkgname))

(define (mypkg-commit? pkg commit)
  (equal?
   (git-reference-commit (origin-uri (package-source pkg)))
   commit
   ))

(define (packages-i-want mypkgname pkg-dir)
  (fold-packages (lambda (package result)
                   (if (mypkg? package mypkgname)
                       (cons package result)
                       result))
                 '()
                 (fold-modules cons '() `(,pkg-dir))))

(define (cigmon-pkg? package)
  (equal? "cigmon" (assoc-ref (package-properties package) "generated-by")))

(define (find-cigmon-packages mypkgname pkg-dir)
  "Returns a list of all cigmon-generated packages inheriting from
<mypkgname> that are defined in <pkg-dir>"
  (fold-packages (lambda (package result)
                   (if (cigmon-pkg? package)
                       (cons package result)
                       result))
                 '()
                 (fold-modules cons '() `(,pkg-dir))))

;; just use delete-duplicates instead
(define (uniquely-add lst1 lst2)
  "This is just a shorthand way of running delete-duplicates on lst1 and then append lst1 to lst2. For each element in lst1 add it to lst2 unless it's already in lst2. Duplicates already in lst2 will remain in the returned list, but duplicates in lst1 will not."
  (if (equal? (length lst1) 0)
      lst2
      (if (not (member (car lst1) lst2))
	  (uniquely-add (list-tail lst1 1) (cons (car lst1) lst2))
	  (uniquely-add (list-tail lst1 1) lst2))))

(define (find-cigmon-packages* pkglist modpath)
  "Returns a space-separated string of all cigmon-generated packages
that are inheriting from a package in <pkglist> that are defined in
<modpath>"
  (string-join
   (delete-duplicates
    (map (lambda (x)
	   (let ((pkgs (map package-name
			    (find-cigmon-packages x modpath))))
	     (string-join pkgs)))
	 pkglist))))

;; not used?
(define (pkg-commits pkg pkg-mods)
  (map (lambda (x)
         (git-reference-commit (origin-uri (package-source x))))
       (packages-i-want pkg pkg-mods)))

;; not used?
(define (commit-exists? pkg pkg-mods commit)
  (pkg-commits pkg pkg-mods))

(define (package-i-want mypkgname pkg-dir commit)
  (fold-packages (lambda (package result)
                   (if (mypkg? package mypkgname)
                       (if (mypkg-commit? package commit)
                           (cons package result)
                           result)
                       result))
                 '()
                 (fold-modules cons '() `(,pkg-dir))))

(define (latest-package-exists? pkg-string pkg-dir)
  "Assuming that package modules in pkg-dir are loaded"
  (let* ((pkg-name (car (string-split pkg-string #\@)))
	 (branch (cadr (string-split pkg-string #\@)))
	 (commit (package-latest-commit-for-branch (car (packages-i-want pkg-name pkg-dir)) branch))
	 (pkg-list (package-i-want pkg-name pkg-dir commit))
	 )
    (if (not (> (length pkg-list) 0))
	;; does not exist
	#f
	;; exists already
	#t)))

(define (load-modules-dir modules-path)
  (let ((dirstream (opendir modules-path))
        (dir modules-path))
    (add-to-load-path modules-path)
    (while #t
      (let ((entry (readdir dirstream)))
        (if (string? entry)
            (if (string-match ".*\\.scm$" entry)
                (let ((file (string-append dir "/" entry)))
                  (display (string-append "Loading file: " file "\n"))
                  (primitive-load file))
                (display (string-append "Not loading directory entry: " entry "\n")))
            (break))))
    (closedir dirstream)))

(define (write-package-def pkg-name commit pkg-dir)
		       (let* ((commit7 (string-take commit 7))
			     (name (string-append pkg-name "-" commit7))
			     (pkg (car (packages-i-want pkg-name pkg-dir)))
			     (pkg-loc (dirname (location-file (package-location pkg))))
			     (url (package-url pkg)))
			 (define pkg-string
			   (string-append "(define-public " name
					  " (package (inherit " pkg-name ")"
					  "(properties '((\"generated-by\" . \"cigmon\")))"
					  "(name \"" name "\")"
					  "(source (origin (method git-fetch) "
					  "(uri (git-reference "
					  "(commit \"" commit "\")"
					  "(url \"" url "\")))"
					  "(sha256 (base32 \"" (package-hash url commit) "\"))))))"))
			 pkg-string))

(define (write-package-mod pkg)
  "Return a string with appropriate module imports for <pkg>."
  (let* ((mod-name (string-append (package-name pkg) "-revs"))
	 (pkg-loc (basename (location-file (package-location pkg)) ".scm")))
    (string-append "(define-module (" mod-name
		   ") #:use-module (" pkg-loc ") #:use-module (guix packages) #:use-module (guix git-download))")))

(define (check-package pkg-string modpath)
  (let* ((pkg-name (car (string-split pkg-string #\@)))
	 (branch (cadr (string-split pkg-string #\@)))
	 (a-pkg (car (packages-i-want pkg-name modpath)))
	 (commit (package-latest-commit-for-branch a-pkg branch))
         (nada (display commit))
	 (pkg-list (package-i-want (string-append pkg-name "-" (string-take commit 7)) modpath commit)))
    (if (> (length pkg-list) 0)
	;; if package exists, just print the existing package to
	;; stdout
	(display pkg-list)

	;; if it doesn't exist, either append it to an existing file
	;; in the same dir named pkg-name-revs.scm or create it with
	;; module imports and then append it.
	(let* ((file (string-append modpath "/" pkg-name "-revs.scm"))
               (commit7 (string-take commit 7))
               (name (string-append pkg-name "-" commit7))
               (new-branch-def (string-append "(define-public " pkg-name "-" branch
                                                   " (package (inherit " name
                                                   ")(name \"" pkg-name "-" branch "\")))\n")))
	  (if (file-exists? file)
              (substitute* file (( (string-append "^\\(define-public " pkg-name "-" branch ".*")) (string-append (write-package-def pkg-name commit modpath) "\n" new-branch-def)))
	      (let ((output-port (open-file file "a")))
		(display (string-append "file: " file "does not exist"))
		(display (write-package-mod a-pkg) output-port)
		(newline output-port)
		(display (write-package-def pkg-name commit modpath) output-port)
		(newline output-port)
		(display new-branch-def output-port)
		(newline output-port)
		(close-port output-port)))
          #t))))

(define (list-package-names pkg-strings)
  "Given a string that has a space-separated list of
<package>@<branch>, return a list with the string-part <package>"
  (map (lambda (x) (car (string-split x #\@)))
       (string-split pkg-strings #\space)))

(define (change-ending file ending) (string-append (dirname file) "/" (car (string-split (car (list-tail (string-split file #\/) (- (length (string-split file #\/)) 1))) #\.)) ending))

(define (write-package-list pkg-string file)
  (let* ((newfile (change-ending file "-revs.scm"))
	 (output-port (open-file newfile "a")))
    (display (string-append "(define cigmon-packages (list " pkg-string "))") output-port)
    (newline output-port)
    (close-port output-port)
    ;; TODO: use substitute* instead of system here
    (system (string-append "cat " file " | grep -v '(define cigmon-packages ' >> " newfile))
    (system* "mv" newfile file)))

(define (main cmd-line)
  (let ((options (getopt-config-auto cmd-line config)))
    (when (option-ref options 'write)
      (options-write options))
    
    ;; Script start, skip verifying options
    (define modpath (option-ref options 'package-modules))
    (define manpath (option-ref options 'manifest))
    (define packages (option-ref options 'packages))
    (define interval (option-ref options 'interval))
    
    ;; Step 1. load package modules
    (load-modules-dir modpath)
    
    ;; For-each package-string check if the
    ;; package-latest-commit-for-branch can be found in our existing
    ;; package-modules using package-i-want pkg commit, otherwise,
    ;; create the package.
    (while #t
      (let* ((result (map (lambda (x) (check-package x modpath)) (string-split packages #\space))))
        (if (member #t result)
            (begin
              ;; reload modules in modpath with the new packages, then
              ;; add all cigmon packages to the manifest files
              (display "#t is member in result")
              (load-modules-dir modpath)              
              
              (let (( cigmon-packages-stringlist
                      (string-join (map (lambda (x) (string-append "\"" x "\"")) (string-split (find-cigmon-packages* (list-package-names packages) modpath) #\space)))))

                (write-package-list cigmon-packages-stringlist manpath)

                ;; optionally apply git commit and git push
                (let ((current-dir (getcwd)))
                  (when (option-ref options 'git-commit-manifest)
	            (chdir (dirname manpath))
                    (system* "git" "add" manpath)
	            (system* "git" "commit" "-m" "guix-cigmon")
	            (chdir current-dir)))
                
                (let ((current-dir (getcwd)))
                  (when (option-ref options 'git-push-manifest)
	            (chdir (dirname manpath))
	            (system* "git" "push")	
	            (chdir current-dir)))
                
                (let ((current-dir (getcwd)))
                  (when (option-ref options 'git-commit-packages)
	            (chdir modpath)
                    (system* "git" "add" modpath)
	            (system* "git" "commit" "-m" "guix-cigmon")
	            (chdir current-dir)))
                
                (let ((current-dir (getcwd)))
                  (when (option-ref options 'git-push-packages)
	            (chdir modpath)
	            (system* "git" "push")	
	            (chdir current-dir))))
              (sleep interval))
            (sleep interval))))))

(main (command-line))
  
